package sample.webonise.weboniseapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.SimpleAdapter;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;


import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import sample.webonise.weboniseapp.R;
import sample.webonise.weboniseapp.config.Config;
import sample.webonise.weboniseapp.customview.CustomAutoCompleteTextView;
import sample.webonise.weboniseapp.db.DatabaseHandler;
import sample.webonise.weboniseapp.model.PlaceModel;
import sample.webonise.weboniseapp.model.PlacePredictions;

public class MainActivity extends AppCompatActivity {


    String TAG="WEBONISE";


    CustomAutoCompleteTextView atvPlaces;
    DatabaseHandler db;
    boolean isFromDB=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        atvPlaces=(CustomAutoCompleteTextView) findViewById(R.id.atv_places);
        setSupportActionBar(toolbar);

        atvPlaces.setThreshold(1);

        db=new DatabaseHandler(this);




        atvPlaces.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                isFromDB=false;
                getPlacesResponse(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub


            }
        });










    }


    @Override
    protected void onResume() {
        super.onResume();


        ArrayList<PlaceModel> placeArrList=db.getAllPlacesInfo();


        /*
          display drop down for autocomplete textview when there is data
          in database for old searchings
         */
        if(placeArrList!=null  && placeArrList.size()>0) {
            isFromDB=true;
            addDataToDropDown(placeArrList);
            atvPlaces.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        atvPlaces.showDropDown();
                    }
                }
            });

        }

    }

    void getPlacesResponse(String s)
    {

        String url= Config.PLACES_URL_PART_1+s+Config.PLACES_URL_PART_2+getString(R.string.browser_key);

        try {
            URL urlUrl=new URL(url);
           url= urlUrl.toString();
           url=url.replace(" ", "%20");
            Log.i(TAG,url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }








        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("RESPONSE", response.toString());

                        Gson gson=new Gson();
                        PlacePredictions predictions=(PlacePredictions) gson.fromJson(response.toString(),PlacePredictions.class);

                        final ArrayList<PlaceModel> predictionList=predictions.getPredictions();

                        addDataToDropDown(predictionList);

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog

            }
        });


        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjReq);




    }


    void addDataToDropDown(final ArrayList<PlaceModel> placeArrayList)
    {


        final List<HashMap<String, String>> placesList = new ArrayList<HashMap<String,String>>();
        for(int i=0;i<placeArrayList.size();i++)
        {
            PlaceModel model=placeArrayList.get(i);
            Log.d(TAG, model.getDescription());



            HashMap<String, String> place = new HashMap<String, String>();
            place.put("description", model.getDescription());
            place.put("_id",model.getId());
            place.put("reference",model.getReference());
            placesList.add(place);

        }

        String[] from = new String[] { "description"};
        int[] to = new int[] { android.R.id.text1 };

        // Creating a SimpleAdapter for the AutoCompleteTextView


        final SimpleAdapter adapter = new SimpleAdapter(getBaseContext(),placesList, android.R.layout.simple_list_item_1, from, to);

        // Setting the adapter
        atvPlaces.setAdapter(adapter);

         if(!isFromDB)
        atvPlaces.showDropDown();


        atvPlaces.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //adapter.getItem(position);

                HashMap<String, String> place=placesList.get(position);

                PlaceModel model= placeArrayList.get(position);

                db.addPlaceInfo(model);
                Intent i=new Intent(MainActivity.this,MapActivity.class);

                i.putExtra("PLACE_ID",model.getPlace_id());

                startActivity(i);


            }
        });


    }


}
