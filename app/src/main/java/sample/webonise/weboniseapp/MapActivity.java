package sample.webonise.weboniseapp;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import sample.webonise.weboniseapp.R;
import sample.webonise.weboniseapp.config.Config;
import sample.webonise.weboniseapp.model.PhotoInfo;
import sample.webonise.weboniseapp.model.PlacePredictions;

/**
 * Created by Manoj on 9/7/2016.
 *
 */
public class MapActivity extends FragmentActivity implements OnMapReadyCallback {


    private GoogleMap mMap;
    String lat,lon;

      String placeId;

    ProgressDialog pDialog;
    ImageView imgView;
    String ref="";

    ArrayList<String> photoRefArray;
    LinearLayout linearParent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_screen);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        imgView=(ImageView) findViewById(R.id.image);
        linearParent = (LinearLayout)findViewById(R.id.linearParent);
        mapFragment.getMapAsync(this);
        placeId=getIntent().getStringExtra("PLACE_ID");


         getData();





    }


    View insertPhoto(String photoRef){


        String s= Config.PHOTO_URL_PART_1+photoRef+Config.PHOTO_URL_PART_2+getString(R.string.browser_key);
        LinearLayout layout = new LinearLayout(getApplicationContext());
        layout.setLayoutParams(new LinearLayout.LayoutParams(250, 250));
        layout.setGravity(Gravity.CENTER);

        ImageView imageView = new ImageView(getApplicationContext());
        imageView.setLayoutParams(new LinearLayout.LayoutParams(220, 220));
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
       // imageView.setImageBitmap(bm);
         getAndSetImage(s,imageView);

        layout.addView(imageView);
        return layout;
    }




    void getAndSetImage(String url, ImageView img)
    {

        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(R.drawable.default_image)
                .showImageOnFail(R.drawable.default_image)
               .showImageOnLoading(R.drawable.default_image).build();
             imageLoader.displayImage(url, img, options);
    }







    void getData()
    {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();

        String url= "https://maps.googleapis.com/maps/api/place/details/json?placeid="+placeId+"&key=AIzaSyA01DlN3HiCzQulZrW0dj7bUlkzEPZREcc";



         JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("RESPONSE", response.toString());

                        Gson gson=new Gson();
                        PlacePredictions predictions=(PlacePredictions) gson.fromJson(response.toString(),PlacePredictions.class);

                        try {
                            JSONObject joResult=response.getJSONObject("result");
                            JSONObject joGeometry=joResult.getJSONObject("geometry");
                            JSONObject joLocation=joGeometry.getJSONObject("location");

                            lat=joLocation.getString("lat");
                            lon=joLocation.getString("lng");

                            JSONArray jAPhotos=joResult.getJSONArray("photos");
                            photoRefArray=new ArrayList<String>();


                            for(int i=0;i<jAPhotos.length();i++)
                            {
                                JSONObject joPhoto=jAPhotos.getJSONObject(i);

                                PhotoInfo photoInfo=(PhotoInfo) gson.fromJson(joPhoto.toString(),PhotoInfo.class);
                                 Log.i("***photo reference",photoInfo.getPhoto_reference());
                                photoRefArray.add(photoInfo.getPhoto_reference());


                                ref=photoInfo.getPhoto_reference();
                            }


                            for(int i=0;i<photoRefArray.size();i++)
                            {
                                linearParent.addView(insertPhoto(photoRefArray.get(i)));
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                        pDialog.hide();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("RESPONSE", "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            }
        });


        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjReq);




    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney, Australia, and move the camera.
        double latD=Double.parseDouble(lat);
        double lonD=Double.parseDouble(lon);

        LatLng place = new LatLng(latD, lonD);
        mMap.addMarker(new MarkerOptions().position(place));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(place));
    }


}
