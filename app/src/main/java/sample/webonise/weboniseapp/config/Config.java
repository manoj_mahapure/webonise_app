package sample.webonise.weboniseapp.config;

/**
 * Created by HP on 9/14/2016.
 */
public class Config {

    public static final String PLACES_URL_PART_1="https://maps.googleapis.com/maps/api/place/autocomplete/json?input=";

    public static final String PLACES_URL_PART_2="&types=geocode&language=en&key=";

    public static final String PHOTO_URL_PART_1="https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=";

    public static final String PHOTO_URL_PART_2="&key=";

}
