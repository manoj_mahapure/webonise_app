package sample.webonise.weboniseapp.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import sample.webonise.weboniseapp.model.PlaceModel;

/**
 * Created by Manoj on 9/13/2016.
 *
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "PlacesDB";

    // Contacts table name
    private static final String TABLE_PLACE_INFO = "PlaceInfo";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_PLACE_ID = "place_id";
    private static final String KEY_PLACE_DESCRIPTION = "place_description";
    private static final String KEY_PLACE_REFERENCE = "place_reference";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_PLACE_INFO + "("
                + KEY_ID + " TEXT PRIMARY KEY," + KEY_PLACE_ID + " TEXT,"
                + KEY_PLACE_DESCRIPTION + " TEXT"+ ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLACE_INFO);

        // Create tables again
        onCreate(db);
    }



    public void addPlaceInfo(PlaceModel placeModel) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, placeModel.getId());
        values.put(KEY_PLACE_ID, placeModel.getPlace_id());
        values.put(KEY_PLACE_DESCRIPTION, placeModel.getDescription());
       // values.put(KEY_PLACE_REFERENCE, placeModel.getReference());



        // Inserting Row
        db.insert(TABLE_PLACE_INFO, null, values);
        db.close(); // Closing database connection
    }


    public ArrayList<PlaceModel> getAllPlacesInfo() {
        ArrayList<PlaceModel> contactList = new ArrayList<PlaceModel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_PLACE_INFO;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                PlaceModel placeModel = new PlaceModel();
                placeModel.setId(cursor.getString(0));
                placeModel.setPlace_id(cursor.getString(1));
                placeModel.setDescription(cursor.getString(2));
              //  placeModel.setReference(cursor.getString(3));

                // Adding contact to list
                contactList.add(placeModel);
            } while (cursor.moveToNext());
        }

        // return contact list
        return contactList;
    }




}