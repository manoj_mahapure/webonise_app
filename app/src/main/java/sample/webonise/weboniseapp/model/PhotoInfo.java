package sample.webonise.weboniseapp.model;

/**
 * Created by Manoj on 9/7/2016.
 */
public class PhotoInfo {

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getPhoto_reference() {
        return photo_reference;
    }

    public void setPhoto_reference(String photo_reference) {
        this.photo_reference = photo_reference;
    }

    private String height;
    private String width;
    private String photo_reference;






}
