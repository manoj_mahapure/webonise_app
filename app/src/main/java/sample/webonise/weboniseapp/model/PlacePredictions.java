package sample.webonise.weboniseapp.model;

import java.util.ArrayList;

/**
 * Created by Manoj on 9/6/2016.
 */
public class PlacePredictions {

    public ArrayList<PlaceModel> getPredictions() {
        return predictions;
    }

    public void setPredictions(ArrayList<PlaceModel> predictions) {
        this.predictions = predictions;
    }

    ArrayList<PlaceModel>  predictions;
}
